@extends('layouts.master')
@section('judul')
Halaman Dashboard
@endsection
@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
        @csrf
        <p>First name:</p>
        <input type="text" id="firstName" name="firstName" autocomplete="off">
        <br>

        <p>Last name:</p>
        <input type="text" id="lastName" name="lastName" autocomplete="off">
        <br>

        <p>Gender:</p>
        <input type="radio" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" value="Other">
        <label for="other">Other</label>
        <br>   

        <p>Nationality:</p>
        <select name="cars" id="cars">
            <option value="Indonesian">Indonesian</option>
            <option value="American">American</option>
            <option value="Australian">Australian</option>
        </select>
        <br>   

        <p>Language Spoken:</p>
        <input type="checkbox" id="bahasaIndonesia" name="language" value="Bahasa Indonesia">
        <label for="Bahasa Indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="English" name="language" value="English">
        <label for="English">English</label><br>
        <input type="checkbox" id="Other" name="language" value="Other">
        <label for="Other">Other</label>
        <br>

        <p>Bio:</p>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection