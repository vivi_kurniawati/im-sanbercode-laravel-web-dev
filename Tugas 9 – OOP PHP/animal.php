<?php

class Animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    public function __construct($name) {
        $this->name = $name;
    }
}

class Frog extends Animal {
    public function jump() {
        echo "Jump : Hop Hop <br>";
    }
}

class Ape extends Animal {
    public function yell() {
        echo "Yell : Auooo <br>";
    }
}

?>