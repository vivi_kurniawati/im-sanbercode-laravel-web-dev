@extends('layouts.master')
@section('judul')
Edit Cast
@endsection
@section('content')
<form method="post" action="/cast/{{ $castdata->id }}">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value ="{{ $castdata->nama }}" placeholder="Enter nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" value ="{{ $castdata->umur }}" placeholder="Enter umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" id="bio" name="bio" rows="4" placeholder="Enter bio">{{ $castdata->bio }}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
