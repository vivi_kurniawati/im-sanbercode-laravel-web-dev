@extends('layouts.master')
@section('judul')
Tampil Cast
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@push('scripts')
    <script src="{{ asset('/template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush
@section('content')
    <h1>Detail Cast {{ $castdata->nama }}</h1>

        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
          </tr>
          </thead>
          <tbody>
                <tr>
                    <td>{{$castdata->nama}}</td>
                    <td>{{$castdata->umur}}</td>
                    <td>{{$castdata->bio}}</td>
                </tr>
            </tbody>
          </tfoot>
        </table>
@endsection