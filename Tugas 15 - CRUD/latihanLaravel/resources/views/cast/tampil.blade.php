@extends('layouts.master')
@section('judul')
Tampil Cast
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@push('scripts')
    <script src="{{ asset('/template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush
@section('content')
    <a class="btn btn-primary my-3" href="/cast/create" role="button">Tambah Kategori</a>

        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="post">
                            @csrf
                            @method('delete')
                            <a class="btn btn-info btn-sm" href="/cast/{{ $item->id }}" role="button">Detail</a>
                            <a class="btn btn-warning btn-sm" href="/cast/{{ $item->id }}/edit" role="button">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
            @endforelse
          </tfoot>
        </table>
@endsection