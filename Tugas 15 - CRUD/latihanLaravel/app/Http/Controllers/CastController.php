<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }
     public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),
        ]);

        return redirect('/cast');
    }
    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.tampil', ['cast' => $cast]);
    }
    public function show($id)
    {
        $castdata = DB::table('cast')->find($id);

        return view('cast.detail', ['castdata' => $castdata]);
    }
    public function edit($id)
    {
        $castdata = DB::table('cast')->find($id);

        return view('cast.edit', ['castdata' => $castdata]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required',
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio'),
            ]);

        return redirect('/cast');
    }
    public function destroy($id)
    {
        $deleted = DB::table('cast')->where('id', '=', $id)->delete();
        
        return redirect('/cast');
    }

}
